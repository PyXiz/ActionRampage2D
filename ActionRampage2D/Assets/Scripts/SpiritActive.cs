﻿using System.Collections;
using UnityEngine;

public class SpiritActive : MonoBehaviour {
    public GameObject DeathEffect;
    public GameObject TextPop;
    public UnitState State;
    public float ActiveRadius;
    public float AttackRadius;
    public float FollowRadius;
    public float MoveSpeed;

    public Vector3 BasePoint { get; set; }
    public int AttackPoint { get; set; }
    public bool LockMobs { get; set; }

    public Transform ClosestEnemy {
        get {
            if (LockMobs) {
                Transform closemob = null;
                float close = Mathf.Infinity;
                var enemies = GameObject.FindGameObjectsWithTag("Mobs");
                foreach (var mob in enemies) {
                    float distance = Vector3.Distance(transform.position, mob.transform.position);
                    if (distance < close) {
                        close = distance;
                        closemob = mob.transform;
                        LockMobs = false;
                    }
                }
                return closemob;
            } else {
                return enemy;
            }
        }
    }

    public int DamageTaken {
        set {
            player.GetComponent<PlayerActive>().Handler.DamageCondition(value / 2);
        }
    }

    public void DistanceMoving() {
        enemy = ClosestEnemy;
        try {
            float basedistance = Vector3.Distance(player.position, transform.position);
            float mobdistance = Vector3.Distance(enemy.position, player.position);

            if (mobdistance <= ActiveRadius && mobdistance > AttackRadius) {
                SpiritMoving(enemy.position);
                if (mobdistance > ActiveRadius) {
                    SpiritMoving(player.position);
                }
            } else {
                if (basedistance > FollowRadius) {
                    SpiritMoving(player.position);
                    LockMobs = false;
                } else {
                    LockMobs = true;
                }
            }

            if (Vector3.Distance(enemy.position, transform.position) <= AttackRadius) {
                spiritanime.SetBool("IsMoving", false);
                if (State != UnitState.Attack) {
                    StartCoroutine(AttackCoroutine());
                }
            }
        } catch (System.Exception) {
            SpiritMoving(player.position);
            LockMobs = true;
        }
    }

    private IEnumerator AttackCoroutine() {
        State = UnitState.Attack;
        spiritanime.SetBool("IsAttacking", true);
        yield return null;

        spiritanime.SetBool("IsAttacking", false);
        yield return new WaitForSeconds(2f);

        var battlepoint = player.GetComponent<PlayerActive>().Handler.Player.BattlePoint;
        State = UnitState.Idle;
        if (battlepoint <= 0) {
            Destroy(gameObject);
            SpiritKilled();
        }
    }

    private void SpiritMoving(Vector3 position) {
        var mobmoving = Vector3.MoveTowards(transform.position, position, MoveSpeed * Time.deltaTime);
        ChangeDirection(mobmoving - transform.position);

        this.position.x = position.x;
        this.position.y = position.y;
        
        spiritrig.MovePosition(mobmoving);
        spiritanime.SetBool("IsMoving", true);
    }

    private void ChangeDirection(Vector2 direction) {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) {
            if (direction.x > 0) {
                SetAnimFloat(Vector2.right);
            } else if (direction.x < 0) {
                SetAnimFloat(Vector2.left);
            }
        } else if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y)) {
            if (direction.y > 0) {
                SetAnimFloat(Vector2.up);
            } else if (direction.y < 0) {
                SetAnimFloat(Vector2.down);
            }
        }
    }

    private void SetAnimFloat(Vector2 vector) {
        spiritanime.SetFloat("MoveX", vector.x);
        spiritanime.SetFloat("MoveY", vector.y);
    }

    private Rigidbody2D spiritrig;
    private Animator spiritanime;
    private Transform player;
    private Transform enemy;
    private Vector3 position;

    private void SpiritKilled() {
        if (DeathEffect != null) {
            var effect = Instantiate(DeathEffect, transform.position, Quaternion.identity);
            Destroy(effect, 2f);
        }
    }

    private void SpiritModified() {
        AttackPoint = player.GetComponent<PlayerActive>().Handler.Player.AttackPoint / 2;
    }

    private void Start() {
        LockMobs = true;
        spiritrig = GetComponent<Rigidbody2D>();
        spiritanime = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
        SpiritModified();
    }

    private void FixedUpdate() {
        var position = Vector3.zero;
        DistanceMoving();
        
        if (this.position == Vector3.zero) {
            spiritanime.SetBool("IsMoving", false);
        }
        this.position = position;
    }
}
