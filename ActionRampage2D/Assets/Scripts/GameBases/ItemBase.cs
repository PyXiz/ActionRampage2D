﻿public class ItemBase {
    public ItemBase() {
        Amount = 1;
    }

    public ItemBase(uint amount) {
        Amount = amount;
    }

    public ItemCatalog Catalog { get; set; }
    public uint Amount { get; set; }

    public virtual int Uid { get; protected set; }
    public virtual string Name { get; protected set; }
    public virtual string Description { get; protected set; }
}
