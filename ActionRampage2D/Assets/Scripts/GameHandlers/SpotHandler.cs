﻿using UnityEngine;

public class SpotHandler : MonoBehaviour {
    public GameObject SoldierMobs;
    public float SpawnTime;

    private GameObject soldiermob;
    private float spawnsoldier;

    private void SoldierHandler(bool flag) {
        if (flag) {
            soldiermob = Instantiate(SoldierMobs, transform.position, Quaternion.identity);
            soldiermob.GetComponent<MobsSoldier>().BasePoint = transform.position;
            spawnsoldier = SpawnTime;
        } else {
            spawnsoldier -= Time.deltaTime;
            if (spawnsoldier <= 0) {
                soldiermob = Instantiate(SoldierMobs, transform.position, Quaternion.identity);
                soldiermob.GetComponent<MobsSoldier>().BasePoint = transform.position;
                spawnsoldier = SpawnTime;
            }
        }
    }

    private void Start() {
        SoldierHandler(true);
    }

    private void Update() {
        if (!soldiermob) {
            SoldierHandler(false);
        }
    }
}
