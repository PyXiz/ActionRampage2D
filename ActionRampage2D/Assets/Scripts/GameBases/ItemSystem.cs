﻿using System;
using System.Linq;
using System.Collections.Generic;

public sealed class ItemSystem {
    public ItemSystem() {
        inventory = new List<ItemBase>();
    }

    public List<ItemBase> InventoryItem() {
        if (inventory == null) {
            inventory = new List<ItemBase>();
        }
        return inventory;
    }

    public ItemBase InventoryItem(int id) {
        return inventory.Where(model => model.Uid.Equals(id)).SingleOrDefault();
    }

    public List<ItemBase> LootsItem(EnemySet type) {
        loottable = new List<ItemBase>();
        switch (type) {
            case EnemySet.Guardian:
                loottable.Add(new MagicStone_Item());
                loottable.Add(new RedElixir_Item());

                lootsqueue = new Loots[3];
                lootsqueue[0] = new Loots() { Chance = 10, Loot = new GreenLeaf_Item() };
                lootsqueue[1] = new Loots() { Chance = 40, Loot = new BluePowder_Item() };
                lootsqueue[2] = new Loots() { Chance = 60, Loot = new MagicStone_Item() };
                loottable.Add(LootProb());

                lootsqueue = new Loots[2];
                lootsqueue[0] = new Loots() { Chance = 40, Loot = new RedElixir_Item() };
                lootsqueue[1] = new Loots() { Chance = 60, Loot = new MagicStone_Item() };
                loottable.Add(LootProb());
                break;
            case EnemySet.Worker:
                lootsqueue = new Loots[3];
                lootsqueue[0] = new Loots() { Chance = 30, Loot = new BluePowder_Item() };
                lootsqueue[1] = new Loots() { Chance = 40, Loot = new RedElixir_Item() };
                lootsqueue[2] = new Loots() { Chance = 60, Loot = new MagicStone_Item() };
                loottable.Add(LootProb());
                break;
            case EnemySet.Soldier:
                loottable.Add(new MagicStone_Item());

                lootsqueue = new Loots[3];
                lootsqueue[0] = new Loots() { Chance = 30, Loot = new BluePowder_Item() };
                lootsqueue[1] = new Loots() { Chance = 40, Loot = new MagicStone_Item() };
                lootsqueue[2] = new Loots() { Chance = 60, Loot = new RedElixir_Item() };
                loottable.Add(LootProb());
                break;
        }
        return loottable;
    }

    public void CreateItem(ItemBase item) {
        bool itemexists = false;
        foreach (var curritem in inventory) {
            if (curritem.Catalog == item.Catalog) {
                curritem.Amount += 1;
                itemexists = true;
            }
        }
        if (!itemexists) {
            inventory.Add(item);
            //OnItemReady?.Invoke(item, EventArgs.Empty);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void DeleteItem(ItemBase item, uint amount = 1) {
        ItemBase itemexists = null;
        foreach (var curritem in inventory) {
            if (curritem.Catalog == item.Catalog) {
                curritem.Amount -= amount;
                itemexists = curritem;
            }
        }
        if (itemexists != null && itemexists.Amount < 1) {
            inventory.Remove(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    //public event EventHandler OnItemReady;
    public event EventHandler OnItemListChanged;

    private struct Loots {
        public ItemBase Loot;
        public int Chance;
    }

    private List<ItemBase> inventory;
    private List<ItemBase> loottable;
    private Loots[] lootsqueue;

    private ItemBase LootProb() {
        int nProb = 0;
        int currProb = UnityEngine.Random.Range(0, 100);
        for (int i = 0; i < lootsqueue.Length; i++) {
            nProb += lootsqueue[i].Chance;
            if (currProb <= nProb) {
                return lootsqueue[i].Loot;
            }
        }
        return null;
    }
}
