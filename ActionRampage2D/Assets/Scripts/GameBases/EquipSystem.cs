﻿public sealed class EquipSystem {
    public IUnit Owner { get; set; }

    public IEquipment MainWeapon { get; set; }
    public IEquipment SubWeapon { get; set; }
    public IEquipment ArmorSet { get; set; }

    public SettingAbility SetAbility { get; set; }

    public bool ItemConsumtion(int slot) {
        var player = Owner as PlayerBase;
        if (player.State == UnitState.Idle && slot <= player.Inventory.InventoryItem().Count) {
            var item = player.Inventory.InventoryItem()[slot - 1] as IConsumable; // PocketSlot[slot - 1];
            if (item != null) {
                item.Effect(Owner);
                player.Inventory.DeleteItem(item as ItemBase);
            }
            return true;
        } else {
            return false;
        }
    }

    public bool ItemConsumtion(IConsumable item) {
        item.Effect(Owner);
        return true;
    }

    public IEquipment EquipRouting(int skillslot) {
        IEquipment selected = null;
        switch (skillslot) {
            case 1:
                selected = MainWeapon;
                break;
            case 2:
                selected = SubWeapon;
                break;
            case 3:
                selected = ArmorSet;
                break;
        }
        return selected;
    }

    public void UseEquipment(IEquipment item) {
        switch (item.ItemType) {
            case ItemType.MainWeapon:
                MainWeapon = item;
                break;
            case ItemType.SubWeapon:
                SubWeapon = item;
                break;
            case ItemType.SetArmor:
                ArmorSet = item;
                break;
        }
        SetAbility(item, true);
        item.BonusStats(Owner, true);
    }

    public void LossEquipment(IEquipment item) {
        switch (item.ItemType) {
            case ItemType.MainWeapon:
                MainWeapon = null;
                break;
            case ItemType.SubWeapon:
                SubWeapon = null;
                break;
            case ItemType.SetArmor:
                ArmorSet = null;
                break;
        }
        SetAbility(item, false);
        item.BonusStats(Owner, false);
    }
}

