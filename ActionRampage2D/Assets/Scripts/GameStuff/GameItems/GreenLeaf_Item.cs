﻿public class GreenLeaf_Item : ItemBase, IConsumable {
    public GreenLeaf_Item() : base() {
        Catalog = ItemCatalog.GreenLeaf;
    }

    public GreenLeaf_Item(uint amount) : base(amount) {
        Catalog = ItemCatalog.GreenLeaf;
    }

    public override int Uid => (int)ItemCatalog.GreenLeaf;
    public override string Name => "Magic Paper";
    public override string Description => "Dapat digunakan untuk mencatat proses permainan";

    public bool CanStock => true;

    public void Effect(IUnit target) {
        target.GamePoint += 100;
    }
}
