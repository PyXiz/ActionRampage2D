﻿using UnityEngine.UI;
using UnityEngine;

public class UI_MarketHandler : MonoBehaviour {
    public static UI_MarketHandler UI { get; private set; }

    public PlayerBase Player { private get; set; }

    public void MarketTrading(TradingAction stock, TradingAction equip, TradingAction codex) {
        this.stock = stock;
        this.equip = equip;
        this.codex = codex;
    }

    public void BtnYes_Click() {
        confirmaction(slot, Player);
        pnlconfirm.gameObject.SetActive(false);
    }

    public void BtnNo_Click() {
        slot = 0;
        confirmaction = null;
        pnlconfirm.gameObject.SetActive(false);
    }

    public void BtnStock1_Click() {
        slot = 1;
        confirmaction = stock;
        pnlconfirm.gameObject.SetActive(true);
    }

    public void BtnStock2_Click() {
        slot = 2;
        confirmaction = stock;
        pnlconfirm.gameObject.SetActive(true);
    }

    public void BtnEquip1_Click() {
        slot = 3;
        confirmaction = equip; 
        pnlconfirm.gameObject.SetActive(true);
    }

    public void BtnEquip2_Click() {
        slot = 4;
        confirmaction = equip;
        pnlconfirm.gameObject.SetActive(true);
    }

    public void BtnEquip3_Click() {
        slot = 5;
        confirmaction = equip; 
        pnlconfirm.gameObject.SetActive(true);
    }

    public void BtnHiStat1_Click() {
        slot = 6;
        confirmaction = codex; 
        pnlconfirm.gameObject.SetActive(true);
    }

    public void BtnHiStat2_Click() {
        slot = 7;
        confirmaction = codex; 
        pnlconfirm.gameObject.SetActive(true);
    }

    private TradingAction stock;
    private TradingAction equip;
    private TradingAction codex;
    private TradingAction confirmaction;
    private Image pnlconfirm;
    private int slot;

    private void Awake() {
        UI = this;
    }

    private void Start() {
        pnlconfirm = transform.parent.Find("PnlConfirm").GetComponent<Image>();
    }
}
