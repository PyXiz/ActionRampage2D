﻿using UnityEngine;

public class BoxSkillHandler : MonoBehaviour {
    private MobsActive target;
    private AbilityHandler ability;
    private UnitHandler unit;
    private int count;
    private bool isdeal;

    private void DealDamage() {
        foreach (var damage in ability.Skill.Effect()) {
            target.DamageTaken(damage, ability.Skill.Cost, DamageState.PlayerMag);
        }
        isdeal = true;
    }
       
    private void OnDisable() {
        if (count > 0 && !ability.Skill.HitArea) {
            DealDamage();
        }
        count = 0;
        if (isdeal) {
            unit.Player.EnergyPoint = -ability.Skill.Cost;
            ability.CasterEquip.Durability -= (uint)ability.Skill.Cost;
            if (ability.CasterEquip.Durability <= 0) {
                ability.EquipmentExpired();
            }
        }
        isdeal = false;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Mobs") && collision.isTrigger) {
            target = collision.GetComponent<MobsActive>();
            unit = target.Target.GetComponent<PlayerActive>().Handler;
            count++;
            if (ability.Skill.HitArea && count >= 1) {
                DealDamage();
            }
        }
    }

    private void Start() {
        ability = transform.parent.parent.GetComponent<AbilityHandler>();
        count = 0;
        isdeal = false;
    }
}
