﻿using System.Collections.Generic;

public class BattleGear_Item : ItemBase, IEquipment {
    public BattleGear_Item() : base() {
        Catalog = ItemCatalog.LongSword;
        Durability = 1000;
    }

    public BattleGear_Item(uint amount, uint durability) : base(amount) {
        Durability = durability;
    }

    public override int Uid => (int)ItemCatalog.BattleGear;
    public override string Name => "Battle Gear";
    public override string Description => "Armor besi, menambahkan 50 DEF, 100 HP, 50 EP dan mengaktifkan skill Battle Cry";

    public uint Durability { get; set; }

    public ItemType ItemType => ItemType.SetArmor;

    public List<SkillBase> HasAbility() {
        return new List<SkillBase> {
            new BattleCry_Skill(),
        };
    }

    public void BonusStats(IUnit target, bool value) {
        if (value) {
            target.DefendPoint = 50;
            target.HealthPoint = 100;
            target.EnergyPoint = 50;
        } else {
            target.DefendPoint = -50;
            target.HealthPoint = -100;
            target.EnergyPoint = -50;
        }
    }
}
