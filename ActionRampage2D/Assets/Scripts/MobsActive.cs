﻿using System;
using UnityEngine;

public abstract class MobsActive : MonoBehaviour {
    public GameObject ItemLoot;
    public GameObject DeathEffect;
    public GameObject TextPop;
    public UnitState State;

    public int HealthPoint { get; set; }
    public int MaxHealthPoint { get; set; }
    public int AttackPoint { get; set; }
    public int DefendPoint { get; set; }
    public Transform Target { get; set; }

    public virtual void DamageTaken(int amount, int cost, DamageState state) {
        if (transform) {
            var deal = amount - Mathf.RoundToInt((float)DefendPoint / 4);
            HealthPoint -= deal;
            DamageHandler.InstanceDamage(TextPop.transform, transform.position, deal, state);
            if (HealthPoint <= 0) {
                Destroy(gameObject);
                EnemyKilled();
            }
        }
    }

    protected Rigidbody2D enemyrig;
    protected Animator enemyanime;
    protected Transform spirit;

    protected virtual void ChangeDirection(Vector2 direction) {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) {
            if (direction.x > 0) {
                SetAnimFloat(Vector2.right);
            } else if (direction.x < 0) {
                SetAnimFloat(Vector2.left);
            }
        } else if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y)) {
            if (direction.y > 0) {
                SetAnimFloat(Vector2.up);
            } else if (direction.y < 0) {
                SetAnimFloat(Vector2.down);
            }
        }
    }

    protected virtual void SetAnimFloat(Vector2 vector) {
        enemyanime.SetFloat("MoveX", vector.x);
        enemyanime.SetFloat("MoveY", vector.y);
    }

    protected abstract void EnemyKilled();
    protected abstract void EnemyModified();
    protected abstract void EnemyMoving();

    private void Start() {
        enemyrig = GetComponent<Rigidbody2D>();
        enemyanime = GetComponent<Animator>();
        Target = GameObject.FindWithTag("Player").transform;
        EnemyModified();
    }

    private void FixedUpdate() {
        EnemyMoving();
    }
}
