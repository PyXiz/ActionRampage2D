﻿using UnityEngine;

public class SkillActive : MonoBehaviour {
    public void Skill_OnCastingBegin() {
        unit.Handler.transform.GetComponent<SpriteRenderer>().enabled = false;
        unit.PlayerRig.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void Skill_OnCastingEnd() {
        unit.Handler.transform.GetComponent<SpriteRenderer>().enabled = true;
        unit.PlayerRig.constraints = RigidbodyConstraints2D.None;
        unit.PlayerRig.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    private PlayerActive unit;

    private void Start() {
        unit = transform.parent.parent.GetComponent<PlayerActive>();
    }
}
