﻿using System;
using System.Collections;
using UnityEngine;

public abstract class PlayerBase : IUnit {
    public UnitState State { get; set; }
    public Animator PlayAnime { get; set; }
    public Transform Body { get; set; }
    public Action<Vector3> DeathVisualEffect { get; set; }

    public ItemSystem Inventory { get; set; }
    public EquipSystem Equipment { get; set; }
    public SkillSystem Skill { get; set; }
    public bool UniqueState { get; set; }

    public int GamePoint { get; set; }

    public int BattlePoint {
        get {
            return btlpoint;
        }
        set {
            btlpoint += value;
            if (btlpoint > GameConst.MAX_BPOINT) {
                btlpoint = GameConst.MAX_BPOINT;
            } else if (btlpoint < 0) {
                btlpoint = 0;
            }
        }
    }

    public int MaxHealthPoint {
        get {
            return maxhelpoint;
        }
        set {
            maxhelpoint += value;
            if (maxhelpoint > GameConst.MAX_HEALTH) {
                maxhelpoint = GameConst.MAX_HEALTH;
            }
        }
    }

    public int MaxEnergyPoint {
        get {
            return maxenepoint;
        }
        set {
            maxenepoint += value;
            if (maxenepoint > GameConst.MAX_ENERGY) {
                maxenepoint = GameConst.MAX_ENERGY;
            }
        }
    }

    public int HealthPoint {
        get {
            return helpoint;
        }
        set {
            helpoint += value;
            if (helpoint > MaxHealthPoint) {
                helpoint = MaxHealthPoint;
            } else if (helpoint < 0) {
                helpoint = 0;
            }
        }
    }

    public int EnergyPoint {
        get {
            return enepoint;
        }
        set {
            enepoint += value;
            if (enepoint > MaxEnergyPoint) {
                enepoint = MaxEnergyPoint;
            } else if (enepoint < 0) {
                enepoint = 0;
            }
        }
    }

    public int AttackPoint {
        get {
            return atkpoint;
        }
        set {
            atkpoint += value;
            if (atkpoint > GameConst.MAX_ATK) {
                atkpoint = GameConst.MAX_ATK;
            }
        }
    }

    public int DefendPoint {
        get {
            return defpoint;
        }
        set {
            defpoint += value;
            if (defpoint > GameConst.MAX_DEF) {
                defpoint = GameConst.MAX_DEF;
            }
        }
    }

    public abstract int Uid { get; }
    public abstract string Name { get; }
    public abstract string Description { get; }
    public abstract IEquipment StarterWeapon { get; }

    public abstract IEnumerator AttackCoroutine();
    public abstract void SetCharacter();

    public virtual void StarterItem() {
        Inventory.CreateItem(new MagicStone_Item(3));
    }

    protected void ControlAbility(IEquipment item, bool flag) {
        var collection = item.HasAbility();
        if (collection == null) return;
        foreach (var ability in collection) {
            if (flag) {
                Skill.CreateSkill(ability, item);
            } else {
                Skill.DeleteSkill(ability);
            }
        }
    }

    private int atkpoint;
    private int defpoint;
    private int maxhelpoint;
    private int maxenepoint;
    private int helpoint;
    private int enepoint;
    private int btlpoint;
}

