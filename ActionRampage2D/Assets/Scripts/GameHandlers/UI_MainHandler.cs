﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class UI_MainHandler : MonoBehaviour {
    public GameObject ItemObject;
    public GameObject ItemGenerator;
    public ItemEquiped ItemDurability;

    public struct ItemEquiped {
        public uint MainWeapon;
        public uint SubWeapon;
        public uint ArmorSet;
    };

    public static UI_MainHandler UI { get; private set; }

    public PlayerBase PlayerStatus { get; set; }
    public string PlayerName { get; set; }
    public string MarketName { get; set; }
    public bool IsNearMarket { get; set; }

    public SkillCooldown SkillAttack_Handler { get; set; }
    public Action<int> ItemUse_Handler { get; set; }
    public Action BaseAttack_Handler { get; set; }

    public void BtnAttack_Click() {
        BaseAttack_Handler();
    }

    public void BtnSkill1_Click() {
        if (skillcooldown_1 == 0) {
            skillcooldown_1 = SkillAttack_Handler(1);
            txtbuttons1.text = skillcooldown_1.ToString();
        }
    }

    public void BtnSkill2_Click() {
        if (skillcooldown_2 == 0) {
            skillcooldown_2 = SkillAttack_Handler(2);
            txtbuttons2.text = skillcooldown_2.ToString();
        }
    }

    public void BtnSkill3_Click() {
        if (skillcooldown_3 == 0) { 
            skillcooldown_3 = SkillAttack_Handler(3);
            txtbuttons3.text = skillcooldown_3.ToString();
        }
    }

    public void BtnItem_Click() {
        if (itemcooldown == 0) {
            ItemUse_Handler(slotitemactive + 1);
            var generator = ItemGenerator.GetComponent<ItemHandler>();
            imgitem.sprite = generator.SpriteSelected(itemactive.Catalog);
            txtqty.text = $"x{itemactive.Amount}";
            itemcooldown = 6;
        }
    }

    public void BtnSwitch_Click() {
        var inventory = PlayerStatus.Inventory.InventoryItem();
        var generator = ItemGenerator.GetComponent<ItemHandler>();
        itemactive = ItemSlotActive(inventory, itemactive.Uid);
        imgitem.sprite = generator.SpriteSelected(itemactive.Catalog);
        txtqty.text = $"x{itemactive.Amount}";
    }

    public void BtnAction_Click() {
        if (IsNearMarket) {
            ispaused = !ispaused;
            if (MarketName == "MarketAlpha") {
                pnlmarket_a.gameObject.SetActive(ispaused);
            } else if (MarketName == "MarketBeta") {
                pnlmarket_b.gameObject.SetActive(ispaused);
            } else if (MarketName == "MarketCharlie") {
                pnlmarket_c.gameObject.SetActive(ispaused);
            }
            Time.timeScale = ispaused ? 0f : 1f;
        }
    }

    public void BtnInfo_Click() {
        ispaused = !ispaused;
        pnlpause.gameObject.SetActive(ispaused);
        Time.timeScale = ispaused ? 0f : 1f;
    }

    public void BtnEnd_Click() {
        
    }

    private ItemBase itemactive;
    private int slotitemactive;
    private float skillcooldown_1;
    private float skillcooldown_2;
    private float skillcooldown_3; 
    private float itemcooldown;
    private bool ispaused;

    private TextMeshProUGUI txtname;
    private TextMeshProUGUI txtmaindura;
    private TextMeshProUGUI txtsubdura;
    private TextMeshProUGUI txtarmordura; 
    private TextMeshProUGUI txtqty;
    private TextMeshProUGUI txtitem;
    private TextMeshProUGUI txthp;
    private TextMeshProUGUI txtsp;
    private TextMeshProUGUI txtgp_a;
    private TextMeshProUGUI txtgp_b;
    private TextMeshProUGUI txtgp_c;

    private Text txtbuttons1;
    private Text txtbuttons2;
    private Text txtbuttons3;
    
    private Slider bargp;
    private Slider barhp;
    private Slider barsp;
    private Slider barbp;

    private Button btnskill1;
    private Button btnskill2;
    private Button btnskill3;
    private Button btnitem;

    private Image pnlmarket_a;
    private Image pnlmarket_b;
    private Image pnlmarket_c;
    private Image pnlpause;
    private Image btnskill1_mask;
    private Image btnskill2_mask;
    private Image btnskill3_mask;
    private Image imgitem;

    private void InitializeComponent() {
        bargp = transform.Find("BarGP").GetComponent<Slider>();
        barhp = transform.Find("BarHP").GetComponent<Slider>();
        barsp = transform.Find("BarSP").GetComponent<Slider>();
        barbp = transform.Find("BarBP").GetComponent<Slider>();

        txthp = barhp.transform.Find("TxtBar").GetComponent<TextMeshProUGUI>();
        txtsp = barsp.transform.Find("TxtBar").GetComponent<TextMeshProUGUI>();

        var panel = transform.Find("PnlSkill").GetComponent<Image>();
        btnskill1 = panel.transform.Find("BtnSkill1").GetComponent<Button>();
        btnskill2 = panel.transform.Find("BtnSkill2").GetComponent<Button>();
        btnskill3 = panel.transform.Find("BtnSkill3").GetComponent<Button>();
        btnskill1_mask = panel.transform.Find("BtnSkill1").GetComponent<Image>();
        btnskill2_mask = panel.transform.Find("BtnSkill2").GetComponent<Image>();
        btnskill3_mask = panel.transform.Find("BtnSkill3").GetComponent<Image>();
        txtbuttons1 = btnskill1.transform.Find("TxtCd1").GetComponent<Text>();
        txtbuttons2 = btnskill2.transform.Find("TxtCd2").GetComponent<Text>();
        txtbuttons3 = btnskill3.transform.Find("TxtCd3").GetComponent<Text>();

        btnitem = transform.Find("BtnItem").GetComponent<Button>();
        imgitem = btnitem.transform.Find("ImgItem").GetComponent<Image>();
        txtqty = btnitem.transform.Find("TxtQty").GetComponent<TextMeshProUGUI>();
        txtitem = btnitem.transform.Find("TxtCd").GetComponent<TextMeshProUGUI>();

        var paneldura = transform.Find("PnlDurable").GetComponent<Image>();
        txtmaindura = paneldura.transform.Find("TxtMain").GetComponent<TextMeshProUGUI>();
        txtsubdura = paneldura.transform.Find("TxtSub").GetComponent<TextMeshProUGUI>();
        txtarmordura = paneldura.transform.Find("TxtArmor").GetComponent<TextMeshProUGUI>();
        txtname = transform.Find("PnlInfo").Find("TxtName").GetComponent<TextMeshProUGUI>();

        pnlpause = transform.Find("PnlPause").GetComponent<Image>();
        pnlmarket_a = transform.Find("PnlMarket_A").GetComponent<Image>();
        pnlmarket_b = transform.Find("PnlMarket_B").GetComponent<Image>();
        pnlmarket_c = transform.Find("PnlMarket_C").GetComponent<Image>();
        txtgp_a = pnlmarket_a.transform.Find("TxtGamePoint").GetComponent<TextMeshProUGUI>();
        txtgp_b = pnlmarket_b.transform.Find("TxtGamePoint").GetComponent<TextMeshProUGUI>();
        txtgp_c = pnlmarket_c.transform.Find("TxtGamePoint").GetComponent<TextMeshProUGUI>();

        btnskill1_mask.color = Color.black;
        btnskill2_mask.color = Color.black;
        btnskill3_mask.color = Color.black;
    }

    private ItemBase ItemSlotActive(List<ItemBase> inventory, int current) {
        var n = 0;
        for (int i = 0; i < inventory.Count; i++) {
            if (inventory[i].Uid == current) {
                n = i;
            }
        }
        if (current == 0 && inventory.Count > 0) {
            slotitemactive = 0;
            return inventory[0];
        } else if (n == inventory.Count - 1 && inventory.Count > 0) {
            slotitemactive = 0;
            return inventory[0];
        }
        return inventory[slotitemactive = ++n];
    }

    private void MonitorSkill() {
        var player = PlayerStatus;
        btnskill1_mask.color = (player.Equipment.MainWeapon != null) ? Color.green : Color.black;
        btnskill2_mask.color = (player.Equipment.SubWeapon != null) ? Color.yellow: Color.black;
        btnskill3_mask.color = (player.Equipment.ArmorSet != null) ? Color.red : Color.black;
    }

    private void MonitorBar() {
        txthp.text = $"{PlayerStatus.HealthPoint} / {PlayerStatus.MaxHealthPoint}";
        txtsp.text = $"{PlayerStatus.EnergyPoint} / {PlayerStatus.MaxEnergyPoint}";

        barhp.value = PlayerStatus.HealthPoint;
        barsp.value = PlayerStatus.EnergyPoint;
        barbp.value = PlayerStatus.BattlePoint;
        bargp.value = PlayerStatus.GamePoint;

        barhp.maxValue = PlayerStatus.MaxHealthPoint;
        barsp.maxValue = PlayerStatus.MaxEnergyPoint;
        barbp.maxValue = GameConst.MAX_BPOINT;
        bargp.maxValue = GameConst.MAX_GPOINT;

        txtgp_a.text = $"Player GP: {PlayerStatus.GamePoint}";
        txtgp_c.text = txtgp_b.text = txtgp_a.text;
    }

    private void MonitorDurability() {
        txtmaindura.text = $"{ItemDurability.MainWeapon / (float)GameConst.MAX_DURABILITY * 100}%";
        txtsubdura.text = $"{ItemDurability.SubWeapon / (float)GameConst.MAX_DURABILITY * 100}%";
        txtarmordura.text = $"{ItemDurability.ArmorSet / (float)GameConst.MAX_DURABILITY * 100}%";
    }

    private void Awake() {
        UI = this;
        InitializeComponent();
        slotitemactive = 0;
        ispaused = false;
    }

    private void Update() {
        if (Input.GetButtonDown("Attack")) {
            BaseAttack_Handler();
        } else if (Input.GetButtonDown("Fire1")) {
            BtnSkill1_Click();
        } else if (Input.GetButtonDown("Fire2")) {
            BtnSkill2_Click();
        } else if (Input.GetButtonDown("Fire3")) {
            BtnSkill3_Click();
        } else if (Input.GetButtonDown("Using")) {
            BtnItem_Click();
        } else if (Input.GetButtonDown("Submit")) {
            BtnAction_Click();
        } else if (Input.GetButtonDown("Switch")) {
            BtnSwitch_Click();
        } else if (Input.GetButtonDown("Cancel")) {
            BtnInfo_Click();
        }

        if (skillcooldown_1 > 1) {
            skillcooldown_1 -= Time.deltaTime;
            txtbuttons1.text = ((int)skillcooldown_1).ToString();
        }

        if (skillcooldown_1 < 1) {
            skillcooldown_1 = 0;
            txtbuttons1.text = string.Empty;
        }

        if (skillcooldown_2 > 1) {
            skillcooldown_2 -= Time.deltaTime;
            txtbuttons2.text = ((int)skillcooldown_2).ToString();
        }

        if (skillcooldown_2 < 1) {
            skillcooldown_2 = 0;
            txtbuttons2.text = string.Empty;
        }

        if (skillcooldown_3 > 1) {
            skillcooldown_3 -= Time.deltaTime;
            txtbuttons3.text = ((int)skillcooldown_3).ToString();
        }

        if (skillcooldown_3 < 1) {
            skillcooldown_3 = 0;
            txtbuttons3.text = string.Empty;
        }

        if (itemcooldown > 1) {
            itemcooldown -= Time.deltaTime;
            txtitem.text = ((int)itemcooldown).ToString();
        }

        if (itemcooldown < 1) {
            itemcooldown = 0;
            txtitem.text = string.Empty;
        }
    }

    private void LateUpdate() {
        if (txtname.text == "") {
            var inventory = PlayerStatus.Inventory.InventoryItem();
            var generator = ItemGenerator.GetComponent<ItemHandler>();
            itemactive = ItemSlotActive(inventory, slotitemactive = 0);
            imgitem.sprite = generator.SpriteSelected(itemactive.Catalog);
            txtqty.text = $"x{itemactive.Amount}";
            txtname.text = PlayerStatus.Name;
        }
        MonitorSkill();
        MonitorBar();
        MonitorDurability();
    }
}
