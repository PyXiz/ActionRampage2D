﻿public class RedElixir_Item : ItemBase, IConsumable {
    public RedElixir_Item() : base() {
        Catalog = ItemCatalog.RedElixir;
    }

    public RedElixir_Item(uint amount) : base(amount) {
        Catalog = ItemCatalog.RedElixir;
    }

    public override int Uid => (int)ItemCatalog.RedElixir;
    public override string Name => "Elixir of Life";
    public override string Description => "Menyembuhkan luka hasil pertempuran, efek menambah 100 hp";

    public bool CanStock => true;

    public void Effect(IUnit target) {
        target.HealthPoint = 100;
    }
}
