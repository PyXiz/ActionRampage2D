﻿public class MentalityCodex_Item : ItemBase, IConsumable {
    public MentalityCodex_Item() : base() {
        Catalog = ItemCatalog.MentalityCodex;
    }

    public MentalityCodex_Item(uint amount) : base(amount) {
        Catalog = ItemCatalog.MentalityCodex;
    }

    public override int Uid => (int)ItemCatalog.MentalityCodex;
    public override string Name => "Codex of Mentality";
    public override string Description => "Instan efek menambah 50 maximum ep";

    public bool CanStock => false;

    public void Effect(IUnit target) {
        target.MaxEnergyPoint = 50;
    }
}