﻿using System.Collections;
using UnityEngine;

public class DarkKnight : PlayerBase {
    public override int Uid => (int)CharacterSet.DarkKnight;
    public override string Name => "Knight";
    public override string Description => "Mantan prajurit elit keluarga kerajaan yang akgirnya dikucilkan. " +
                                          "Datang ke Battle Cry Island sebagai pemburu bayaran. " +
                                          "Petarung jarak dekat, handal menggunakan pedang melawan musuh tunggal hingga kelompok, " +
                                          "dan dilengkapi dengan extra pedang panjang untuk membubarkan musuh yang berkumpul";
    public override IEquipment StarterWeapon => new LongSword_Item();

    public override IEnumerator AttackCoroutine() {
        if (State != UnitState.Cast && Equipment.MainWeapon != null) {
            State = UnitState.Attack;
            PlayAnime.SetBool("IsAttacking", true);
            yield return new WaitForSeconds(0.4f); //--> Attack Speed

            PlayAnime.SetBool("IsAttacking", false);
            yield return null;
            //yield return new WaitForSeconds(0.7f);

            State = UnitState.Idle;
        }
    }

    public override void SetCharacter() {
        var anim = Resources.Load("Animations/Player/Darkknight") as RuntimeAnimatorController;
        //var sprite = Resources.Load<Sprite>("player_0");
        //Body.GetComponent<SpriteRenderer>().sprite = sprite;
        Body.GetComponent<Animator>().runtimeAnimatorController = anim;

        MaxHealthPoint = 100;
        MaxEnergyPoint = 60;
        HealthPoint = 100;
        EnergyPoint = 60;
        AttackPoint = 0;
        DefendPoint = 0;
        GamePoint = 500;
        BattlePoint = 15;

        Inventory = new ItemSystem();
        Equipment = new EquipSystem { 
            Owner = this, 
            SetAbility = ControlAbility, 
        };
        Skill = new SkillSystem();
    }
}

