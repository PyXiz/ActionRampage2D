﻿using System.Linq;
using System.Collections.Generic;

public class Market {
    public int Uid { get; set; }
    public string Name { get; set; }
    public int Lock { get; set; }
    public int Price { get; set; }
    public ItemBase Trade { get; set; }
    public ItemBase Product { get; set; }
    public bool Slot { get; set; }
}

public sealed class MarketSystem {
    public MarketSystem() {
        collection = new List<Market> {
            new Market { Uid = 1, Name = "Magic Stone Premium", Lock = 250, Price = 50, Trade = null, Product = new MagicStone_Item(), Slot = false },
            new Market { Uid = 2, Name = "Green Leaf Premium", Lock = 250, Price = 100, Trade = null, Product = new GreenLeaf_Item(), Slot = false },
            new Market { Uid = 3, Name = "High Grade Sword", Lock = 500, Price = 300, Trade = new MagicStone_Item(10), Product = new LongSword_Item(), Slot = false },
            new Market { Uid = 4, Name = "Sword of Slayer", Lock = 1000, Price = 750, Trade = new MagicStone_Item(20), Product = new GreatSword_Item(), Slot = false },
            new Market { Uid = 5, Name = "The Battle Gear", Lock = 2000, Price = 1000, Trade = new MagicStone_Item(20), Product = new BattleGear_Item(), Slot = false },
            new Market { Uid = 6, Name = "Codex of Vitality", Lock = 500, Price = 0, Trade = new RedElixir_Item(10), Product = new VitalityCodex_Item(), Slot = false },
            new Market { Uid = 7, Name = "Codex of Mentality", Lock = 500, Price = 0, Trade = new BluePowder_Item(10), Product = new MentalityCodex_Item(), Slot = false },
        };
    }

    public ItemBase[] ProductMarket {
        get {
            var product = new ItemBase[7];
            for (int i = 0; i < product.Length; i++) {
                product[i] = collection[i].Product;
            }
            return product;
        }
    }

    public int[] TradingSlot {
        get {
            var slot = new int[collection.Count];
            for (int i = 0; i < collection.Count; i++) {
                slot[i] = collection[i].Lock;
            }
            return slot;
        }
        set {
            for (int i = 0; i < value.Length; i++) {
                if (value[i] == collection[i].Lock) {
                    collection[i].Slot = true;
                }
            }
        }
    }

    public IEnumerable<Market> DataMarket() {
        return collection;
    }

    public Market DataMarket(int id) {
        return collection.Where(model => model.Uid.Equals(id)).SingleOrDefault();
    }

    public void TradingStock(int slot, PlayerBase player) {
        var product = DataMarket(slot);
        if (player.GamePoint >= product.Price) {
            player.GamePoint -= product.Price;
            player.Inventory.CreateItem(product.Product);
        }
    }

    public void TradingEquipment(int slot, PlayerBase player) {
        var product = DataMarket(slot);
        var itemtrade = player.Inventory.InventoryItem(product.Trade.Uid);
        if (player.GamePoint >= product.Price && itemtrade.Amount >= product.Trade.Amount) {
            var equip = product.Product as IEquipment;
            equip.Durability = 1000;
            player.GamePoint -= product.Price;
            player.Inventory.DeleteItem(itemtrade, product.Trade.Amount);
            player.Equipment.UseEquipment(equip);
        }
    }

    public void TradingCodex(int slot, PlayerBase player) {
        var product = DataMarket(slot);
        var itemtrade = player.Inventory.InventoryItem(product.Trade.Uid);
        if (itemtrade.Amount >= product.Trade.Amount) {
            player.Equipment.ItemConsumtion(product.Product as IConsumable);
            player.Inventory.DeleteItem(itemtrade, product.Trade.Amount);
        }
    }

    private readonly List<Market> collection;
}
