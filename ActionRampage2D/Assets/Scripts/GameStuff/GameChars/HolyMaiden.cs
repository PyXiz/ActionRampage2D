﻿using System.Collections;
using UnityEngine;

public class HolyMaiden : PlayerBase {
    public override int Uid => (int)CharacterSet.HolyMaiden;
    public override string Name => "Maiden";
    public override string Description => "Pemimpin temple Altar of Life. Lahir dari kaum bangsawan, memiliki tekad yang kuat " +
                                          "Datang ke Battle Cry Island untuk membasmi semua iblis yang kian berkembang. " +
                                          "Menggunakan cahaya untuk melawan kejahatan, Hebat dalam memberikan dukungan dalam pertempuran " +
                                          "Ditemani dengan para petarung hebat dari rasnya. ";
    public override IEquipment StarterWeapon => new LongSword_Item();

    public override IEnumerator AttackCoroutine() {
        if (State != UnitState.Cast && Equipment.MainWeapon != null) {
            State = UnitState.Attack;
            PlayAnime.SetBool("IsAttacking", true);
            yield return null;

            PlayAnime.SetBool("IsAttacking", false);
            yield return new WaitForSeconds(0.4f);

            if (spirit.gameObject.activeSelf) {
                spirit.gameObject.SetActive(hasspirit = false);
                DeathVisualEffect(spirit.position);
            } else {
                spirit.gameObject.SetActive(hasspirit = true);
            }
            spirit.GetComponent<SpiritActive>().LockMobs = hasspirit;
            spirit.position = Body.position;
            State = UnitState.Idle;
        }
    }

    public override void SetCharacter() {
        var anim = Resources.Load("Animations/Player/HolyMaiden") as RuntimeAnimatorController;
        var sprite = Resources.Load<Sprite>("players_0");
        Body.GetComponent<SpriteRenderer>().sprite = sprite;
        Body.GetComponent<Animator>().runtimeAnimatorController = anim;

        MaxHealthPoint = 100;
        MaxEnergyPoint = 60;
        HealthPoint = 100;
        EnergyPoint = 60;
        AttackPoint = 0;
        DefendPoint = 0;
        GamePoint = 0;
        BattlePoint = 15;

        Inventory = new ItemSystem();
        Equipment = new EquipSystem {
            Owner = this,
            SetAbility = ControlAbility,
        };
        Skill = new SkillSystem();
        Equipment.UseEquipment(StarterWeapon);

        hasspirit = false;
        spirit = Body.Find("Spirit");
        spirit.position = Body.position;
        spirit.gameObject.SetActive(false);
    }

    private Transform spirit;
    private bool hasspirit;
}
