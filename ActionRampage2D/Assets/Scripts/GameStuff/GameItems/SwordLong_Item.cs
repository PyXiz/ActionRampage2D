﻿using System.Collections.Generic;

public class LongSword_Item : ItemBase, IEquipment {
    public LongSword_Item() : base() {
        Catalog = ItemCatalog.LongSword;
        Durability = 1000;
    }

    public LongSword_Item(uint amount, uint durability) : base(amount) {
        Durability = durability;
    }

    public override int Uid => (int)ItemCatalog.LongSword;
    public override string Name => "Sword of Glory";
    public override string Description => "Pedang kualitas tinggi, menambahkan 50 ATK dan mengaktifkan skill Focus Attack";
    
    public uint Durability { get; set; }

    public ItemType ItemType => ItemType.MainWeapon;

    public List<SkillBase> HasAbility() {
        return new List<SkillBase> {
            new FocusAttack_Skill(),
        };
    }

    public void BonusStats(IUnit target, bool value) {
        target.AttackPoint = value ? 50 : -50;
    }
}
