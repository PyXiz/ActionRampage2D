﻿using System;
using UnityEngine;

public class AbilityHandler : MonoBehaviour {
    public GameObject FocusAttack;
    public GameObject SplashAttack;
    public GameObject BattleCry;

    public SkillBase Skill {
        get {
            return skill;
        }
        set {
            skill = value;
            clone = transform.Find(((SkillCatalog)skill.Uid).ToString() + "(Clone)");
        }
    }

    public IEquipment CasterEquip { get; private set;}

    public float CastSkill(float[] position, int slot) {
        if (Skill.IsSkillReady(cooldownslot[slot - 1])) {
            Skill.EffectAnime = clone.GetComponent<Animator>();
            Skill.EffectAnime.SetFloat("MoveX", position[0]);
            Skill.EffectAnime.SetFloat("MoveY", position[1]);
            StartCoroutine(Skill.CastCoroutine());
            CasterEquip = (Skill.Caster as PlayerBase).Equipment.EquipRouting(slot);
            return cooldownslot[slot - 1] = Skill.CoolDown;
        } else {
            shout = transform.parent.Find("Warning");
            shout.gameObject.SetActive(true);
            timing = 1;
            return 0;
        }
    }

    public void Skill_OnAbilityEquip(IEquipment item, SkillBase skill) {
        if (skill.Uid == (int)SkillCatalog.FocusAttack) {
            Instantiate(FocusAttack, transform.position, Quaternion.identity, transform);
        } else if (skill.Uid == (int)SkillCatalog.SplashAttack) {
            Instantiate(SplashAttack, transform.position, Quaternion.identity, transform);
        } else if (skill.Uid == (int)SkillCatalog.BattleCry) {
            Instantiate(BattleCry, transform.position, Quaternion.identity, transform);
        }
    }

    public void Skill_OnAbilityRemove(SkillBase skill) {
        Skill = skill;
        var selected = clone.gameObject;
        Destroy(selected);
    }

    public void EquipmentExpired() {
        (Skill.Caster as PlayerBase).Equipment.LossEquipment(CasterEquip);
        OnSkillExpired?.Invoke();
    }

    public event Action OnSkillExpired;

    private SkillBase skill;
    private Transform clone;
    private Transform shout;
    private float[] cooldownslot;
    private float timing;

    private void Start() {
        cooldownslot = new float[] { 0, 0, 0 };
    }

    private void Update() {
        if (timing > 0) {
            timing -= Time.deltaTime;
            if (timing <= 0) {
                shout.gameObject.SetActive(false);
                timing = 0;
            }
        }

        if (cooldownslot[0] > 0) {
            cooldownslot[0] -= Time.deltaTime;
        }

        if (cooldownslot[0] < 0) {
            cooldownslot[0] = 0;
        }

        if (cooldownslot[1] > 0) {
            cooldownslot[1] -= Time.deltaTime;
        }

        if (cooldownslot[1] < 0) {
            cooldownslot[1] = 0;
        }

        if (cooldownslot[2] > 0) {
            cooldownslot[2] -= Time.deltaTime;
        }

        if (cooldownslot[2] < 0) {
            cooldownslot[2] = 0;
        }
    }
}
