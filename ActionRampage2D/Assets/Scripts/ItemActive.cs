﻿using UnityEngine;

public class ItemActive : MonoBehaviour {
    public ItemBase Item {
        get {
            return item;
        }
        set {
            item = value;
            var itemsprite = GameObject.FindWithTag("Inventory").GetComponent<ItemHandler>();
            if (item is RedElixir_Item) {
                sprrender.sprite = itemsprite.RedElixirSkin;
            } else if (item is BluePowder_Item) {
                sprrender.sprite = itemsprite.BluePowderSkin;
            } else if (item is GreenLeaf_Item) {
                sprrender.sprite = itemsprite.GreenLeafSkin;
            } else if (item is MagicStone_Item) {
                sprrender.sprite = itemsprite.MagicStoneSkin;
            } else if (item is LongSword_Item) {
                sprrender.sprite = itemsprite.SwordLongSkin;
            } else if (item is GreatSword_Item) {
                sprrender.sprite = itemsprite.SwordGreatSkin;
            } else if (item is BattleGear_Item) {
                sprrender.sprite = itemsprite.BattleGearSkin;
            }
        }
    }
     
    public ItemActive SpawnItem(Vector3 position, ItemBase item) {
        var transform = Instantiate(UI_MainHandler.UI.ItemObject, RandomSpawnPosition(position, 2f), Quaternion.identity);
        var itemspawn = transform.GetComponent<ItemActive>();
        itemspawn.Item = item;
        return itemspawn;
    }

    public void DestroyItem() {
        Destroy(gameObject);
    }

    private SpriteRenderer sprrender;
    private ItemBase item;

    private Vector3 RandomSpawnPosition(Vector3 vector, float range) {
        float x = Random.Range(-range, range);
        float y = Random.Range(-range, range);
        var rand = vector + new Vector3(x, y);
        return rand;
    }

    private void Awake() {
        sprrender = GetComponent<SpriteRenderer>();
    }
}
