﻿using System;
using System.Collections.Generic;
using System.Linq;

public sealed class SkillSystem {
	public SkillSystem() {
        abilitylist = new SkillBase[3];
	}

    public SkillBase[] ReadSkill() {
        return abilitylist;
        //return abilitylist.OrderBy(model => model.Uid).ToArray();
    }

    public SkillBase ReadSkill(int id) {
        for (int i = 0; i < abilitylist.Length; i++) {
            if (abilitylist[i] != null && abilitylist[i].Uid == id) {
                return abilitylist[i];
            }
        }
        return null;
        //return abilitylist.Where(model => model.Uid.Equals(id)).SingleOrDefault();
    }

    public bool CreateSkill(SkillBase skill, IEquipment item) {
        var detail = ReadSkill(skill.Uid);
        if (item.ItemType == ItemType.MainWeapon) {
            if (detail != null) {
                abilitylist[0] = null;
                OnAbilityRemove?.Invoke(detail);
            }
            abilitylist[0] = skill;
        } else if (item.ItemType == ItemType.SubWeapon) {
            if (detail != null) {
                abilitylist[1] = null;
                OnAbilityRemove?.Invoke(detail);
            }
            abilitylist[1] = skill;
        } else if (item.ItemType == ItemType.SetArmor) {
            if (detail != null) {
                abilitylist[2] = null;
                OnAbilityRemove?.Invoke(detail);
            }
            abilitylist[2] = skill;
        }
        //if (detail != null) {
        //    abilitylist.Remove(detail);
        //    OnAbilityRemove?.Invoke(detail);
        //}
        //abilitylist.Add(skill);
        OnAbilityEquip?.Invoke(item, skill);
        OnAbilityListChanged?.Invoke(this, EventArgs.Empty);
        return true;
    }

    public bool DeleteSkill(SkillBase skill) {
        var detail = ReadSkill(skill.Uid);
        if (detail != null) {
            for (int i = 0; i < abilitylist.Length; i++) {
                if (abilitylist[i] == detail) {
                    abilitylist[i] = null;
                }
            }
            //abilitylist.Remove(detail);
            OnAbilityRemove?.Invoke(detail);
            OnAbilityListChanged?.Invoke(this, EventArgs.Empty);
        }        
        return true;
    }

    public event EventHandler OnAbilityListChanged;
    public event Action<IEquipment, SkillBase> OnAbilityEquip;
    public event Action<SkillBase> OnAbilityRemove;

    private readonly SkillBase[] abilitylist;
}
