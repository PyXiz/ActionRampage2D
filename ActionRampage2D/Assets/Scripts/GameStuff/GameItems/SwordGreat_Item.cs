﻿using System.Collections.Generic;

public class GreatSword_Item : ItemBase, IEquipment {
    public GreatSword_Item() : base() {
        Catalog = ItemCatalog.GreatSword;
        Durability = 1000;
    }

    public GreatSword_Item(uint amount, uint durability) : base(amount) {
        Durability = durability;
    }

    public override int Uid => (int)ItemCatalog.GreatSword;
    public override string Name => "Sword of Desctruction";
    public override string Description => "Pedang Legenda, menambahkan 100 ATK, 30 DEF dan mengaktifkan skill Splash Attack";

    public uint Durability { get; set; }

    public ItemType ItemType => ItemType.SubWeapon;

    public List<SkillBase> HasAbility() {
        return new List<SkillBase> {
            new SplashAttack_Skill(),
        };
    }

    public void BonusStats(IUnit target, bool value) {
        if (value) {
            target.AttackPoint = 100;
            target.DefendPoint = 30;
        } else {
            target.AttackPoint = -100;
            target.DefendPoint = -30;
        }
    }
}
