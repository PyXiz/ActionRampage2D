﻿using UnityEngine;

public class UnitHandler : MonoBehaviour {
    public GameObject DeathEffect;
    public GameObject TextPop;
    public UnitState State;
    public CharacterSet Char;

    public PlayerBase Player { get; set; }

    private void DeathVisualEffect(Vector3 target) {
        if (DeathEffect != null) {
            var effect = Instantiate(DeathEffect, target, Quaternion.identity);
            Destroy(effect, 1.06f);
        }
    }

    public virtual void DamageTaken(int amount, int cost, DamageState state) {
        if (transform) {
            var deal = amount - Mathf.RoundToInt((float)Player.DefendPoint / 4);
            Player.HealthPoint -= deal;
            DamageHandler.InstanceDamage(TextPop.transform, transform.position, deal, state);
            if (Player.HealthPoint <= 0) {
                Destroy(gameObject);
                // TombStone
            }
        }
    }

    private void Awake() {
        switch (Char) {
            case CharacterSet.DarkKnight:
                Player = new DarkKnight();
                break;
            case CharacterSet.HolyMaiden:
                Player = new HolyMaiden();
                break;
        }
        Player.Body = transform;
        Player.PlayAnime = GetComponent<Animator>();
        Player.DeathVisualEffect = DeathVisualEffect;
        Player.UniqueState = false;
        Player.SetCharacter();
        Player.StarterItem();
    }

    private void Update() {
        State = Player.State;
    }
}
