﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SkillBase {
    public IUnit Caster { get; set; }
    public Animator CasterAnime { get; set; }
    public Animator EffectAnime { get; set; }

    public abstract int Uid { get; }
    public abstract string Name { get; }
    public abstract string Description { get; }
    public abstract int Cost { get; }
    public abstract float CoolDown { get; }
    public abstract bool HitArea { get; }

    public virtual bool IsSkillReady(double cooldown) {
        return cooldown == 0 && Caster.EnergyPoint >= Cost;
    }

    public abstract IEnumerator CastCoroutine();
    public abstract IEnumerable<int> Effect();
}
