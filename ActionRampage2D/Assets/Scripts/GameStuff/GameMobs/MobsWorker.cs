﻿using UnityEngine;

public class MobsWorker : MobsActive {
    public float ActiveRadius;
    public float AttackRadius;
    public float MoveSpeed;

    public Vector3 BasePoint { get; set; }

    protected override void EnemyMoving() {
        var distance = Vector3.Distance(Target.position, transform.position);

        if (distance <= ActiveRadius && distance > AttackRadius) {
            EnemyWalking(Target.position);
        } else if (distance > ActiveRadius) {
            EnemyWalking(BasePoint);
            if (Vector3.Distance(transform.position, BasePoint) <= AttackRadius) {
                EnemyStance(Target.position);
            }
        }
    }

    protected override void EnemyKilled() {
        if (DeathEffect != null) {
            var effect = Instantiate(DeathEffect, transform.position, Quaternion.identity);
            Destroy(effect, 2f);
        }
        var loot = new ItemSystem();
        foreach (var item in loot.LootsItem(EnemySet.Worker)) {
            if (item != null) {
                ItemLoot.GetComponent<ItemActive>().SpawnItem(transform.position, item);
            }
        }
        Target.GetComponent<PlayerActive>().Handler.Player.GamePoint += 50;
    }

    protected override void EnemyModified() {
        HealthPoint = Target.GetComponent<PlayerActive>().Handler.Player.MaxHealthPoint;
        AttackPoint = Target.GetComponent<PlayerActive>().Handler.Player.AttackPoint / 2;
    }

    private void EnemyWalking(Vector3 position) {
        var mobmoving = Vector3.MoveTowards(transform.position, position, MoveSpeed * Time.deltaTime);
        ChangeDirection(mobmoving - transform.position);
        enemyrig.constraints = RigidbodyConstraints2D.None;
        enemyrig.constraints = RigidbodyConstraints2D.FreezeRotation;
        enemyrig.MovePosition(mobmoving);
        enemyanime.SetBool("IsMoving", true);
    }

    private void EnemyStance(Vector3 position) {
        var mobmoving = Vector3.MoveTowards(transform.position, position, Time.deltaTime);
        ChangeDirection(mobmoving - transform.position);
        enemyanime.SetBool("IsMoving", false);
        enemyrig.constraints = RigidbodyConstraints2D.FreezeAll;
    }
}
