﻿using UnityEngine;

public class PlayerActive : MonoBehaviour {
    public GameObject ItemEffect;
    public int MoveSpeed;

    public UnitHandler Handler { get; set; }
    public Rigidbody2D PlayerRig { get; set; }

    public void MoveOn() {
        if (position != Vector3.zero && Handler.Player.State != UnitState.Cast 
                                     && Handler.Player.State != UnitState.Attack) {
            position.Normalize();
            PlayerRig.velocity = position * MoveSpeed;

            playeranime.SetFloat("MoveX", position.x);
            playeranime.SetFloat("MoveY", position.y);
            playeranime.SetBool("IsMoving", true);
            direction[0] = position.x;
            direction[1] = position.y;
        } else {
            position = new Vector3(0, 0).normalized;
            PlayerRig.velocity = position;
            playeranime.SetBool("IsMoving", false);
        }
    }

    public void Player_BaseAttack() {
        if (Handler.Player.BattlePoint > 0) {
            StartCoroutine(Handler.Player.AttackCoroutine());
        } else {
            shout = transform.Find("Warning");
            shout.gameObject.SetActive(true);
            timing = 1;
        }
    }

    public float Player_SkillCast(int slot) {
        float cooldown = 0f;
        var player = Handler.Player;
        if (player.State != UnitState.Cast && player.Skill.ReadSkill().Length > 0) {
            try {
                var skill = player.Skill.ReadSkill()[slot - 1];
                player.Body.GetComponent<SpriteRenderer>().sprite = null;
                skill.Caster = player;
                skill.CasterAnime = Handler.GetComponent<Animator>();
                if (!skill.CasterAnime.GetBool("IsMoving")) {
                    var ability = transform.Find("Ability").GetComponent<AbilityHandler>();
                    ability.Skill = skill;
                    cooldown = ability.CastSkill(direction, slot);
                }
            } catch (System.Exception) {
                Debug.Log("ERROR: Player_SkillCast");
                cooldown = 0f;
            }
        }
        return cooldown;
    }

    public void Player_ItemUse(int slot) {
        if (Handler.Player.Equipment.ItemConsumtion(slot)) {
            var effect = Instantiate(ItemEffect, transform.position, Quaternion.identity, transform);
            Destroy(effect, 0.8f);
        }
    }

    private MarketSystem market;
    private Animator playeranime;
    private Vector3 position;
    private Transform shout;
    private float timing;
    private float[] direction;
    private bool isuiready;

    private void DebugInfo() {
        string info_1 =
            $"\n~ Character Info {Handler.Player.Name}\n" +
            $"Health {Handler.Player.HealthPoint}/{Handler.Player.MaxHealthPoint}\n" +
            $"Energy {Handler.Player.EnergyPoint}/{Handler.Player.MaxEnergyPoint}\n" +
            $"Attack: {Handler.Player.AttackPoint} - Defend: {Handler.Player.DefendPoint}\n" +
            $"Battle Point {Handler.Player.BattlePoint}\n" +
            $"---------------------------------\n" +
            $"~ Equipment Info \n" +
            //$"Slot Main Weapon : {Handler.Player.Equipment.MainWeapon} - Dura: {Handler.Player.Equipment.MainWeapon.Durability}\n" +
            //$"Slot Sub Weapon  : {Handler.Player.Equipment.SubWeapon}\n" +
            //$"Slot Armor Set   : {Handler.Player.Equipment.ArmorSet}\n" +
            $"---------------------------------\n" +
            $"~ Skill Info\n ";
        string info_2 = "";
        for (int i = 0; i < Handler.Player.Skill.ReadSkill().Length; i++) {
            info_2 += $"Skill {Handler.Player.Skill.ReadSkill()[i].Name} - {i}\n";
        }
        //foreach (var item in Handler.Player.Skill.ReadSkill()) {
            
        //}
        info_2 +=
            $"---------------------------------\n" +
            $"~ Pocket Info \n";
        string info_3 = "";
        foreach (var item in Handler.Player.Inventory.InventoryItem()) {
            info_3 += $"({item.Amount}x) {item.Name}\n";
        }
        info_3 +=
            $"---------------------------------\n" +
            $"Game Point {Handler.Player.GamePoint}\n";
        Debug.Log(info_1 + info_2 + info_3);
    }

    private void Abilityhandler_OnSkillExpired() {
        Handler.transform.GetComponent<SpriteRenderer>().enabled = true;
        PlayerRig.constraints = RigidbodyConstraints2D.None;
        PlayerRig.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Items") && collision.isTrigger) {
            var itemdrop = collision.GetComponent<ItemActive>();
            if (itemdrop != null) {
                Handler.Player.Inventory.CreateItem(itemdrop.Item);
                itemdrop.DestroyItem();
            }
        }

        if (collision.gameObject.CompareTag("Market") && collision.isTrigger) {
            transform.Find("Action").gameObject.SetActive(UI_MainHandler.UI.IsNearMarket = true);
            UI_MainHandler.UI.MarketName = collision.name;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Market") && collision.isTrigger) {
            transform.Find("Action").gameObject.SetActive(UI_MainHandler.UI.IsNearMarket = false);
            UI_MainHandler.UI.MarketName = string.Empty;
        }
    }

    private void Awake() {
        PlayerRig = GetComponent<Rigidbody2D>();
        playeranime = transform.Find("DarkKnight").GetComponent<Animator>();
        Handler = transform.Find("DarkKnight").GetComponent<UnitHandler>();
        /**
         * DarkKnight
         * HolyMaiden
         */
        var ability = transform.Find("Ability").GetComponent<AbilityHandler>();
        ability.OnSkillExpired += Abilityhandler_OnSkillExpired;
        Handler.Player.Skill.OnAbilityEquip += ability.Skill_OnAbilityEquip;
        Handler.Player.Skill.OnAbilityRemove += ability.Skill_OnAbilityRemove;
        Handler.Player.Equipment.UseEquipment(Handler.Player.StarterWeapon);

        direction = new float[2];
        market = new MarketSystem();

        isuiready = false;
    }
     
    private void Update() {
        var position = Vector3.zero;

        position.x = Input.GetAxisRaw("Horizontal");
        position.y = Input.GetAxisRaw("Vertical");
        this.position = position;
        MoveOn();
        
        if (timing > 0) {
            timing -= Time.deltaTime;
            if (timing <= 0) {
                shout.gameObject.SetActive(false);
                timing = 0;
            }
        }

        var ui = UI_MainHandler.UI;
        var trade = UI_MarketHandler.UI;
        var player = Handler.Player;

        if (!isuiready) {
            if (ui) {
                ui.PlayerName = Handler.Player.Name;
                ui.PlayerStatus = Handler.Player;
                ui.BaseAttack_Handler = Player_BaseAttack;
                ui.SkillAttack_Handler = Player_SkillCast;
                ui.ItemUse_Handler = Player_ItemUse;
                isuiready = true;
            }
        }

        ui.ItemDurability.MainWeapon = (player.Equipment.MainWeapon == null) ? 0 : player.Equipment.MainWeapon.Durability;
        ui.ItemDurability.SubWeapon = (player.Equipment.SubWeapon == null) ? 0 : player.Equipment.SubWeapon.Durability;
        ui.ItemDurability.ArmorSet = (player.Equipment.ArmorSet == null) ? 0 : player.Equipment.ArmorSet.Durability;

        trade.Player = player;
        trade.MarketTrading(market.TradingStock, market.TradingEquipment, market.TradingCodex);

        //if (Input.GetMouseButtonDown(1)) {
        //    Player_SkillCast(1);
        //} else if (Input.GetMouseButtonDown(2)) {
        //    Player_SkillCast(2);
        //} else if (Input.GetKeyDown(KeyCode.Alpha5)) {
        //    Player_ItemUse(1);
        //} else if (Input.GetKeyDown(KeyCode.Alpha6)) {
        //    Player_ItemUse(2);
        //} else if (Input.GetKeyDown(KeyCode.Alpha7)) {
        //    Player_ItemUse(3);
        //} else if (Input.GetKeyDown(KeyCode.Alpha8)) {
        //    Player_ItemUse(4);
        //} else if (Input.GetKeyDown(KeyCode.F1)) {
        //    market.TradingStock(1, Handler.Player);
        //} else if (Input.GetKeyDown(KeyCode.F2)) {
        //    market.TradingStock(2, Handler.Player);
        //} else if (Input.GetKeyDown(KeyCode.F3)) {
        //    market.TradingEquipment(3, Handler.Player);
        //} else if (Input.GetKeyDown(KeyCode.F4)) {
        //    market.TradingEquipment(4, Handler.Player);
        //} else if (Input.GetKeyDown(KeyCode.F5)) {
        //    market.TradingEquipment(5, Handler.Player);
        //} else if (Input.GetKeyDown(KeyCode.F6)) {
        //    market.TradingCodex(6, Handler.Player);
        //} else if (Input.GetKeyDown(KeyCode.F7)) {
        //    market.TradingCodex(7, Handler.Player);
        //} else if (Input.GetKeyDown(KeyCode.Escape)) {
        //    //var equip = Handler.Player.Equipment;
        //    //if (equip.MainWeapon != null) {
        //    //    equip.LossEquipment(equip.MainWeapon);
        //    //}
        //    //else 
        //    //if (equip.SubWeapon != null) {
        //    //    equip.LossEquipment(equip.SubWeapon);
        //    //}
        //    DebugInfo();
        //}
    }
}


// Kombat Sistem
// User Interface


