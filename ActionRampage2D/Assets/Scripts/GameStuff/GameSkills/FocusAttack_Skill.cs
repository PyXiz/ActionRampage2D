﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusAttack_Skill : SkillBase {
    public override int Uid => (int)SkillCatalog.FocusAttack;
    public override string Name => "Focus Attack";
    public override string Description => "Melancarkan serangan lurus kedepan!, Menambahkan total 2x ATK point kedalam base attack, melukai musuh yang ada didepan.";
    public override int Cost => 10;
    public override float CoolDown => 10;
    public override bool HitArea => false;

    public override IEnumerator CastCoroutine() {
        if ((Caster as PlayerBase).State != UnitState.Attack) {
            (Caster as PlayerBase).State = UnitState.Cast;
            EffectAnime.SetBool("EffectAnimated", true);
            yield return null;

            EffectAnime.SetBool("EffectAnimated", false);
            yield return new WaitForSeconds(0.75f);

            (Caster as PlayerBase).State = UnitState.Idle;
        }
    }

    public override IEnumerable<int> Effect() {
        var damagecoll = new List<int> {
            Caster.AttackPoint + (Caster.AttackPoint * 2),
        };
        return damagecoll;
    }

    
}
