﻿using System.Collections;

public class MagicStone_Item : ItemBase, IConsumable {
    public MagicStone_Item() : base() {
        Catalog = ItemCatalog.MagicStone;
    }

    public MagicStone_Item(uint amount) : base(amount) {
        Catalog = ItemCatalog.MagicStone;
    }

    public override int Uid => (int)ItemCatalog.MagicStone;
    public override string Name => "Magic Stone";
    public override string Description => "Knight menggunakan batu ini untuk menajamkan kembali pedang, " +
                                          "Maiden menggunakan batu ini untuk membangkitkan pejuang spirit," +
                                          "Ranger memecah batu ini untuk dijadikan amunisi.";

    public bool CanStock => true;

    public void Effect(IUnit target) {
        target.BattlePoint = 15;
    }
}
