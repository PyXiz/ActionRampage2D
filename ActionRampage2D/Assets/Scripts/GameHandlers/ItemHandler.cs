﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHandler : MonoBehaviour {
    public Sprite RedElixirSkin;
    public Sprite BluePowderSkin;
    public Sprite GreenLeafSkin;
    public Sprite MagicStoneSkin;
    public Sprite SwordLongSkin;
    public Sprite SwordGreatSkin;
    public Sprite BattleGearSkin;

    public Sprite SpriteSelected(ItemCatalog catalog) {
        Sprite sprite = null;
        switch (catalog) {
            case ItemCatalog.MagicStone:
                sprite = MagicStoneSkin;
                break;
            case ItemCatalog.RedElixir:
                sprite = RedElixirSkin;
                break;
            case ItemCatalog.BluePowder:
                sprite = BluePowderSkin;
                break;
            case ItemCatalog.GreenLeaf:
                sprite = GreenLeafSkin;
                break;
            case ItemCatalog.LongSword:
                sprite = SwordLongSkin;
                break;
            case ItemCatalog.GreatSword:
                sprite = SwordGreatSkin;
                break;
            case ItemCatalog.BattleGear:
                sprite = BattleGearSkin;
                break;
        }
        return sprite;
    }
}
