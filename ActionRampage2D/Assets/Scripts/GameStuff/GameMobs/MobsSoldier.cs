﻿using UnityEngine;

public class MobsSoldier : MobsActive {
    public float ActiveRadius;
    public float AttackRadius;
    public float ChaseRadius;
    public float MoveSpeed;

    public Vector3 BasePoint { get; set; }

    protected override void EnemyMoving() {
        var distance = Vector3.Distance(Target.position, transform.position);

        if (distance <= ActiveRadius) {
            enemyanime.SetBool("IsRaising", true);
            State = UnitState.Idle;
            if (distance <= ChaseRadius && distance > AttackRadius) {
                if (State == UnitState.Idle) {
                    EnemyWalking(Target.position);
                } else {
                    enemyanime.SetBool("IsRaising", true);
                }
            }
        } else if (distance > ChaseRadius && State == UnitState.Moving) {
            EnemyWalking(BasePoint);
            if (Vector3.Distance(transform.position, BasePoint) <= AttackRadius) {
                enemyanime.SetBool("IsRaising", false);
                State = UnitState.Sleeping;
            }
        } else {
            enemyanime.SetBool("IsRaising", false);
            State = UnitState.Sleeping;
        }
    }

    protected override void EnemyKilled() {
        if (DeathEffect != null) {
            var effect = Instantiate(DeathEffect, transform.position, Quaternion.identity);
            Destroy(effect, 2f);
        }
        var loot = new ItemSystem();
        foreach (var item in loot.LootsItem(EnemySet.Soldier)) {
            if (item != null) {
                ItemLoot.GetComponent<ItemActive>().SpawnItem(transform.position, item);
            }
        }
        Target.GetComponent<PlayerActive>().Handler.Player.GamePoint += 75;
    }

    protected override void EnemyModified() {
        HealthPoint = Target.GetComponent<PlayerActive>().Handler.Player.MaxHealthPoint / 2;
        AttackPoint = Target.GetComponent<PlayerActive>().Handler.Player.AttackPoint;
        DefendPoint = Target.GetComponent<PlayerActive>().Handler.Player.DefendPoint * 2;
        State = UnitState.Sleeping;
    }

    private void EnemyWalking(Vector3 position) {
        var mobmoving = Vector3.MoveTowards(transform.position, position, MoveSpeed * Time.deltaTime);
        ChangeDirection(mobmoving - transform.position);
        enemyrig.constraints = RigidbodyConstraints2D.None;
        enemyrig.constraints = RigidbodyConstraints2D.FreezeRotation;
        enemyrig.MovePosition(mobmoving);
        enemyanime.SetBool("IsMoving", true);
        State = UnitState.Moving;
    }
}
