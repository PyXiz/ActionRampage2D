﻿using UnityEngine;

public class PosHandler : MonoBehaviour {
    public GameObject GuardMobs;
    public GameObject WorkerMobs;
    public float SpawnTime;
    public int CampMobs;

    private GameObject guardmob;
    private GameObject workermob;
    private float spawnguard;
    private float spawnworker;

    private void GuardHandler(bool flag) {
        if (flag) {
            guardmob = Instantiate(GuardMobs, transform.position, Quaternion.identity);
            spawnguard = SpawnTime;
        } else {
            spawnguard -= Time.deltaTime;
            if (spawnguard <= 0) {
                guardmob = Instantiate(GuardMobs, transform.position, Quaternion.identity);
                spawnguard = SpawnTime;
            }
        }
    }

    private void WorkerHandler(bool flag) {
        if (flag) {
            for (int i = 0; i < CampMobs; i++) {
                workermob = Instantiate(WorkerMobs, RandomSpawnPosition(transform.position), Quaternion.identity);
                workermob.GetComponent<MobsWorker>().BasePoint = transform.position;
            }
            spawnworker = SpawnTime;
        } else {
            spawnworker -= Time.deltaTime;
            if (spawnworker <= 0) {
                for (int i = 0; i < CampMobs; i++) {
                    workermob = Instantiate(WorkerMobs, RandomSpawnPosition(transform.position), Quaternion.identity);
                    workermob.GetComponent<MobsWorker>().BasePoint = transform.position;
                }
                spawnworker = SpawnTime;
            }
        }
    }

    private Vector3 RandomSpawnPosition(Vector3 vector) {
        float x = Random.Range(-2.5f, 2.5f);
        float y = Random.Range(-2.5f, 2.5f);
        var rand = vector + new Vector3(x, y);
        return rand;
    }

    private void Start() {
        GuardHandler(true);
        WorkerHandler(true);
    }

    private void Update() {
        if (!guardmob) {
            GuardHandler(false);
        }

        if (!workermob) {
            WorkerHandler(false);
        }
    }
}
