﻿public class VitalityCodex_Item : ItemBase, IConsumable {
    public VitalityCodex_Item() : base() {
        Catalog = ItemCatalog.VitalityCodex;
    }

    public VitalityCodex_Item(uint amount) : base(amount) {
        Catalog = ItemCatalog.VitalityCodex;
    }

    public override int Uid => (int)ItemCatalog.VitalityCodex;
    public override string Name => "Codex of Vitalty";
    public override string Description => "Instan efek menambah 50 maximum hp";

    public bool CanStock => false;

    public void Effect(IUnit target) {
        target.MaxHealthPoint = 50;
    }
}