﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void TradingAction(int slot, PlayerBase player);
public delegate void SettingAbility(IEquipment item, bool flag);
public delegate float SkillCooldown(int slot);

public interface IUnit {
    int HealthPoint { get; set; }
    int EnergyPoint { get; set; }
    int MaxHealthPoint { get; set; }
    int MaxEnergyPoint { get; set; }
    int GamePoint { get; set; }
    int AttackPoint { get; set; }
    int DefendPoint { get; set; }
    int BattlePoint { get; set; }
}

public interface IConsumable {
    bool CanStock { get; } 
    void Effect(IUnit target);
}

public interface IEquipment {
    uint Durability { get; set; }
    ItemType ItemType { get; }
    List<SkillBase> HasAbility();
    void BonusStats(IUnit target, bool value);
}

public struct GameConst {
    public const int MAX_BPOINT = 15;
    public const int MAX_HEALTH = 999;
    public const int MAX_ENERGY = 999;
    public const int MAX_GPOINT = 99999;
    public const int MAX_ATK = 999;
    public const int MAX_DEF = 999;
    public const int MAX_ITEM = 99;
    public const int MAX_DURABILITY = 1000;
}

public class GameManager : MonoBehaviour {
    
}

public enum SkillCatalog : int {
    Null = 0,
    FocusAttack = 1,
    SplashAttack = 2,
    BattleCry = 3,
}

public enum ItemCatalog : int {
    Null = 0,
    MagicStone = 11,
    RedElixir = 12,
    BluePowder = 13,
    GreenLeaf = 14,
    LongSword = 21,
    GreatSword = 22,
    BattleGear = 31,
    VitalityCodex = 41,
    MentalityCodex = 42,
}

public enum CharacterSet {
    DarkKnight,
    HolyMaiden,
}

public enum EnemySet {
    Guardian,
    Worker,
    Soldier,
}

public enum UnitState {
    Idle,
    Active,
    Sleeping,
    Attack,
    Cast,
    Moving,
    Damaged,
    Dead,
}

public enum DamageState {
    PlayerPhs,
    PlayerMag,
    EnemyPhs,
    AllyHeal,
    AllyRest,
}

public enum ItemType {
    MainWeapon,
    SubWeapon,
    SetArmor,
}
