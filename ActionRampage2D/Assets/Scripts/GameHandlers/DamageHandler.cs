﻿using UnityEngine;
using TMPro;

public class DamageHandler : MonoBehaviour {
    public static DamageHandler InstanceDamage(Transform popup, Vector3 position, float amount, DamageState state) {
        var damage = Instantiate(popup, position, Quaternion.identity);
        var damagepop = damage.GetComponent<DamageHandler>();
        damagepop.Setup((int)amount, state);
        return damagepop;
    }

    public void Setup(int damage, DamageState state) {
        TxtPopup.SetText(damage.ToString());
        timer = 1f;
        switch (state) {
            case DamageState.PlayerPhs:
                textcolor = Color.red;
                movevector = new Vector3(1, 1) * 6f;
                break;
            case DamageState.PlayerMag:
                textcolor = Color.blue;
                movevector = new Vector3(1, 1) * 6f;
                break;
            case DamageState.EnemyPhs:
                textcolor = Color.yellow;
                movevector = new Vector3(-1, 1) * 6f;
                break;
            case DamageState.AllyHeal:
                textcolor = Color.green;
                movevector = new Vector3(-1, -1) * 6f;
                break;
            case DamageState.AllyRest:
                textcolor = Color.cyan;
                movevector = new Vector3(-1, -1) * 6f;
                break;
        }
        TxtPopup.color = textcolor;
    }

    private TextMeshPro TxtPopup;
    private Vector3 movevector;
    private Color textcolor;
    private float timer;

    private void Awake() {
        TxtPopup = transform.GetComponent<TextMeshPro>();
    }

    private void Update() {
        transform.position += movevector * Time.deltaTime;
        movevector -= movevector * 4f * Time.deltaTime;

        if (timer > 1f * 0.5f) {
            transform.localScale += Vector3.one * 1f * Time.deltaTime;
        } else {
            transform.localScale -= Vector3.one * 1f * Time.deltaTime;
        }

        timer -= Time.deltaTime;
        if (timer < 0) {
            float timerspeed = 5f;
            textcolor.a -= timerspeed * Time.deltaTime;
            TxtPopup.color = textcolor;
            if (textcolor.a < 0) {
                Destroy(gameObject);
            }
        }
    }
}
