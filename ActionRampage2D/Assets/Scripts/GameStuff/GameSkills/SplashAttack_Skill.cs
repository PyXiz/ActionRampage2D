﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashAttack_Skill : SkillBase {
    public override int Uid => (int)SkillCatalog.SplashAttack;
    public override string Name => "Splash Attack";
    public override string Description => "Melancarkan serangan berputar. Menambahkan 50 ditambah jumlah hp yang hilang sebagai damage. Melukai musuh-musuh dijarak radius serangan";
    public override int Cost => 10;
    public override float CoolDown => 10;
    public override bool HitArea => true;

    public override IEnumerator CastCoroutine() {
        if ((Caster as PlayerBase).State != UnitState.Attack) {
            (Caster as PlayerBase).State = UnitState.Cast;
            EffectAnime.SetBool("EffectAnimated", true);
            yield return null;

            EffectAnime.SetBool("EffectAnimated", false);
            yield return new WaitForSeconds(1.2f);

            (Caster as PlayerBase).State = UnitState.Idle;
        }
    }

    public override IEnumerable<int> Effect() {
        var damagecoll = new List<int>();
        int bonusdamage = Caster.MaxHealthPoint - Caster.HealthPoint / 2;
        damagecoll.Add(50 + bonusdamage);
        return damagecoll;
    }
}