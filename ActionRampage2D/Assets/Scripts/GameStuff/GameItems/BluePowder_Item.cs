﻿public class BluePowder_Item : ItemBase, IConsumable {
    public BluePowder_Item() : base() {
        Catalog = ItemCatalog.BluePowder;
    }

    public BluePowder_Item(uint amount) : base(amount) {
        Catalog = ItemCatalog.BluePowder;
    }

    public override int Uid => (int)ItemCatalog.BluePowder;
    public override string Name => "Exctract Herb";
    public override string Description => "Memulihkan tenaga untuk kembali bertarung, efek menambah 50 ep";

    public bool CanStock => true;

    public void Effect(IUnit target) {
        target.EnergyPoint = 50;
    }
}
