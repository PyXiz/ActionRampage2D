﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleCry_Skill : SkillBase {
    public override int Uid => throw new System.NotImplementedException();
    public override string Name => throw new System.NotImplementedException();
    public override string Description => throw new System.NotImplementedException();
    public override int Cost => 10;
    public override float CoolDown => 1;
    public override bool HitArea => false;

    public override IEnumerator CastCoroutine() {
        throw new System.NotImplementedException();
    }

    public override IEnumerable<int> Effect() {
        throw new System.NotImplementedException();
    }
}