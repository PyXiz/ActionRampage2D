﻿using UnityEngine;

public class CameraActive : MonoBehaviour {
    public Vector2 MinPos;
    public Vector2 MaxPos;
    public float Smoothing;

    private Transform target;

    private void LateUpdate() {
        try {
            target = GameObject.FindWithTag("Player").transform;
            if (transform.position != target.position) {
                var targetpos = new Vector3(target.position.x, target.position.y, transform.position.z);
                targetpos.x = Mathf.Clamp(targetpos.x, MinPos.x, MaxPos.x);
                targetpos.y = Mathf.Clamp(targetpos.y, MinPos.y, MaxPos.y);
                transform.position = Vector3.Lerp(transform.position, targetpos, Smoothing);
            }
        } catch { 
            Debug.Log("CameraActive.LateUpdate");
        }
    }
}
