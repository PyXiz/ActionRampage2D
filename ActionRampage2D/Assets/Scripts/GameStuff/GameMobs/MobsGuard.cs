﻿using UnityEngine;

public class MobsGuard : MobsActive {
    public float ActiveRadius;

    protected override void EnemyMoving() {
        if (Vector3.Distance(Target.position, transform.position) <= ActiveRadius) {
            enemyanime.SetBool("IsRaising", true);
        } else {
            enemyanime.SetBool("IsRaising", false);
        }
    }

    protected override void EnemyKilled() {
        if (DeathEffect != null) {
            var effect = Instantiate(DeathEffect, transform.position, Quaternion.identity);
            Destroy(effect, 2f);
        }
        var loot = new ItemSystem();
        foreach (var item in loot.LootsItem(EnemySet.Guardian)) {
            if (item != null) {
                ItemLoot.GetComponent<ItemActive>().SpawnItem(transform.position, item);
            }
        }
        Target.GetComponent<PlayerActive>().Handler.Player.GamePoint += 150;
    }

    protected override void EnemyModified() {
        HealthPoint = Target.GetComponent<PlayerActive>().Handler.Player.MaxHealthPoint * 2;
        DefendPoint = Target.GetComponent<PlayerActive>().Handler.Player.DefendPoint;
    }
}
