﻿using UnityEngine;

public class BoxHitHandler : MonoBehaviour {
    private int count;

    private void OnDisable() {
        count = 0;
    }

    private void OnCollisionExit2D(Collision2D collision) {
        count = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Mobs") && collision.isTrigger) {
            var target = collision.GetComponent<MobsActive>();
            var player = target.Target.GetComponent<PlayerActive>().Handler.Player;
            if (count == 0 && !player.UniqueState) {
                target.DamageTaken(player.AttackPoint, 0, DamageState.PlayerPhs);
                player.BattlePoint = -1;
            } else if (player.UniqueState) {
                target.DamageTaken(player.AttackPoint, 0, DamageState.PlayerPhs);
                player.BattlePoint = -1;
            }
            count++;
        }
    }

    private void Start() {
        count = 0;
    }
}
